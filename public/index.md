

[TOC]

# Materials

- [Canvas Page](https://auburn.instructure.com/courses/1100213)
- [BookOfProof pdf](./docs/BookOfProof.pdf)
- [Book Of Proof read online](./docs/BookOfProof.html)

# Fundamentals

---



## Axioms
- A basic fact assumed to be true
    - Ex. Between every two points exists a line
### Basic Axioms
- Clousure under addition and multiplication for the integers and the reals.
    - Let $a,b \in \mathbb{Z}$ Then the following are `true`
        - $a+b\in \mathbb{Z}$
        - $a-b\in \mathbb{Z}$
        - $a\cdot{b}\in \mathbb{Z}$
    - The above properties also apply to the reals.

## Sets
### Natural Numbers
- $\mathbb{N}=\{1,2,3,4...\}$
### Integers
- $\mathbb{Z} = \{-5-4-3,1,3,4...\}$
### Rational Numbers
- $\mathbb{Q}=\{\frac{a}b | a,b,\in Z, b \ne{0}\}$
- The set of all values a dived by b, where a and b are integers, and b is not 0.

### Real Numbers
- $\mathbb{R}$

## Therom
- A therom is a mathmatical statement that is true and can be (and has been) proven or verified as `true`
### Example Therom
- Therom: Let $a,b$ be the lengths of the legs of a right triangle, and let $c$ be the length of the hypotnuce. Then $a^2+b^2=c^2$.

## Proofs
- A proof of a therom is a written verification that shows that the therom is definetley `true`.

## Helpful Definitions
- An integer $n$ is **even** if $n=2a$ for some integer $a\in Z$
    - $0$ is even
- An integer is **odd** if $n=2a+1$ for some integer $a\in{Z}$

## Other Terms
- A proposistion is a true statment, but not as significant as a therom. (a minor therom)
- A lemma is a speiacal kind of therom whos main purpose is to help prove another therom.
- A corollary is a reult that is an immediate consequence of another therom.

# Direct Proofs

---



### Outline of direct proof
If $P$ is `true` then $Q$ is `true`
#### Proof
> Proof: Suppose $P$ is true
> Stuff happens, Section 5.3 Book of Proofs}
> herefor, $Q$ is `true`. $\square$

# Chapter 6 Proof By Contradiction
---

## Outline
- Proposistion: $P$ is true
- Proof: Suppose $P$ is false
    - Stuff
    - Therefore $C$ is both true and false, which is a contradiction. $\square$

### Using contradiction to proove conditional statements
- Proposistion If P, then Q
- Proof: Suppose P and not Q
    - Stuff
    - Therefore c and not c are both true, which is a contradiction. 

- Recall that a arational number is a number that can be written in the form $\frac{A}{B}$ where $A,B\in Z$ A number that is not a rational number is called irational
- Recall that a possitive integer is prime if it is only factors are itself and 1. If a natuaral number that is not prime is said to be composiste. 

## Examples
Proposistion: The number $\sqrt{2}$ is irrational.

> Proof: Suppose that $\sqrt{2}$ is actually rational number. Thus $\exists$ $a,b \in Z$ such that $\sqrt{2}=\frac{a}{b}$ Further, assume that $\frac{a}{b}$ is fully reduced. if $\sqrt{2}=\frac{a}b$ than squaring both sides gives $2=\frac{a^2}{b^2}$ or $2=2b^2$ Thus $a^2$ is even. We have already shown that if $a^2$ is even, then $a$ must be even. Then by definition, there exists an integer $c$ such that $a=2c$. Plugging this in to $a^2=2b^2$ gives $2b^2=(2c)^2=4c^2$ or $b^2=2c^2$ Therefore $b^2$ and thus $b$ are also even. This is a contradiction, since if both $a,b$ are even, then $\frac{a}b$ isn't fully reduced. Thus our assumption that $\sqrt{2}$ must be rational is wrong, therefore $\sqrt{2}$ is irational. $\square$

Proposistion: There are an infinite amount of prime numbers. 

> Proof: Suppose there are finitely many prime numbers, say $P_1, P_2,...P_k$ where $k$ is the finite number of primes. Consider the number $c=P_1P_2P_3...P_k+1$. Clearly $c$ is larger than any prime number and must therefore be composite. Since $c$ is composite, it must have at least one prime factor, say $P_n$. Where $P_n$ is some prime in the list of primes. Thus $c=P_n\cdot{}z$, so $P_n\cdot{}z=P_1P_2P_3...P_k+1$ Clearly $P_n|c$ Also we note that $P_n|_1,P_2,P_3,...P_k$, since $P_n$ is one of those primes. So we can wirte $P_1P_2P_3...P_k=P_n\cdot{x}$ for $x \in Z$. Subsituting into c=P_1P_2P_3...P_k+1 gives $P_nz=P_nx+1$ Subratacting $P_Nx$ from both sides gives $P_nz-P_nx=1$ or $P_n(z-x)=1$ Thus $P_n|1$ wich is impossible since $P_n>1$ thus our initial assumption must have been wrong, therefore there are infinietly many primes. $\square$

Propisistion: Suppose that $a$ is an integer, if $a^2$ is even, then $a$ is even. 
> Proof: Let $a \in Z$. Suppose for the sake of contradiction that $a^2$ is even, and $a$ is not even. Thus $a$ is odd. Since $a$ is odd there exists $c \in Z$ such that $a=2c+1$ Now $a^2=(2c+1)^2=c^2+4c+1$. So $a^2$ is odd.  Thus $a^2$ is both even and not even, which is a contradiction. $\square$

Proposistion: If $r$ is a possitive rational number, then $r$ can be expressed as the product of two irrational numbers. 

>Proof: Suppose that $r$ is a possitive rational number, so $r = \frac{a}{b}$ where $a,b \in Z^+$. Certainly we can write $r$ as $r=\sqrt{2}\cdot{}\frac{r}{\sqrt{2}}$ we know that $\sqrt{2}$ is irrational. We need to show that $\frac{r}{\sqrt{2}}$ is irrational. 
>o show this, lets assume for the sake of ocontradiction, that $\frac{r}{\sqrt{2}}$ is rational. so $\frac{r}{\sqrt{2}}=\frac{c}{d}$ for $c,d \in Z$. Multiplying both sides by $\sqrt{2}$ gives $r=\frac{c}{d}\sqrt{2}$ We know that $r=\frac{a}{b}$ this $\frac{a}{b}=\frac{c}{d}\sqrt{2}$. Multiplying both sides by $\frac{d}{c}$ gives that $\frac{ad}{bc}=\sqrt{2}$. Note that $b\ne0$ since $r$ is rational, and $c\ne0$ since $\frac{r}{\sqrt{2}}>0$ Thus $\sqrt{2}=\frac{ab}{cd}$ implies $\sqrt{2}$ is rational which contradicts out knowledge that $\sqrt{2}$ is rational. Thus $\frac{r}{\sqrt{2}}$ must be irational. Consequently $r=\sqrt{2}\cdot{}\frac{r}{\sqrt{2}}$ is a product of two irrational numbers. $\square$

## Homework
- BOP Pg. 118 1-12 , 15-18 (done by contradiction)

1. Suppose n∈Z. If n is odd, then $n^2$ is odd.

> Proof: Suppose $n$ is an odd number and $n\in Z$. For a contradiction, let say that $n^2$ is even. This means that
>
> Thus if $n$ is odd, then $n^2$ must also be odd. $\square$

12. Prop: For every positive $x\in Z$, there is a positive $y\in Q$ for where $y<x$.
> Proof: Suppose that there is a possitive $x\in Q$ such that there is no possitive $y\in Q$ with $y < x$. In other words, $y \ge x$ for all possitive $y\in Q$. Consider the rational number $y=\frac{x}2$. Clearly y is possitive since x is positive and $y<x$, which contradicts our assumption that x is the smallest rational number. Therefore such an x must not exist. $\square$